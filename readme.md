# OpenLayers + ol/geom

Este ejemplo muestra cómo se pueden añadir capas vectoriales en formato GeoJSON.

Primero se crea una mapa base utilizando la clase ``ol.layer.Tile``. Esta clase se encarga de cargar y mortrar los datos de mapas del proyecto OpenStreetMap, utilizando la clase ``ol.source.OSM`` que especifica la fuente de los datos. Para definir la vista inicial con propiedades especificas, se configura utilizando la clase ``ol.View``. Esta clase permite especificar cómo se muestra inicialmente el mapa en terminos de ubicación y zoom.

```js 
const map = new Map({
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
    vectorLayer,
  ],
  target: 'map',
  view: new View({
    center: [-99.12766, 19.42847],
    projection: "EPSG:4326",
    zoom: 13.5
  }),
})
```

Para mostrar datos vectoriales, se utiliza la clase ``ol.layer.Vector``. Esta clase permite añadir al mapa las capas vectoriales, que son colecciones de geometrias (puntos, líneas, polígonos, multilíneas, multipolígono o un conjunto de todas las geometrias). Para leer el formato vectorial, se pueden usar los formatos: **_GeoJSON_**, **_KML_** o **_TopoJSON_**, en este caso se usa ``ol.source.GeoJSON`. Esta fuente lee y carga los datos GeoJSONpara que puedan ser mostrados en el mapa.

 El aspecto visual de los datos vectoriales está representado por la clase ``ol.style.Style`` que tiene modificar las propiedades incluyendo el color de relleno (``fill``)el color y ancho del borde (``stroke``), el texto (``text``) o imagenes (``image) que se aplicarán. 